# Project Title

Legends of Strength and Wizardry - Caracter Creation System

## Description

In this console application you are able to create an RPG character and items such as armor and weapons. Charaters can equip items and level up, and stats will automatically update accordingly. There are four character classes: Mage, Ranger, Rouge, and Warrior. Each class has different attributes and item affinities. This is not a complete system with working UI but a base system to implement into any projects. 

## Getting Started
Clone the project and modify it how you please to make it fit your own project.

### Dependencies

* Visual Studio Community 2022 (using .NET 6)
* Windows 10

### Installing

* Clone repository and open with Visual Studio. 

## Help

Repport any issues here on Gitlab.

## Authors

Contributors names and contact info

Marcus Jarlevid  
* Gitlab: [@Jallemania](https://gitlab.com/Jallemania)
* GitHub: [@Jallelainen](https://github.com/Jallelainen)
* LinkedIn: [@Marcus Jarlevid](https://www.linkedin.com/in/marcus-jarlevid)

## Version History

* 0.1
    * Initial Release

## Acknowledgments

* This project was developed as an assignment for a course in C# and .NET. Course provided by Noroff Education AS.
