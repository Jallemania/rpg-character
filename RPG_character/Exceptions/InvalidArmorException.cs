﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPG_character.Exceptions
{
    [Serializable]
    public class InvalidArmorException : Exception
    {
        public InvalidArmorException()
        {

        }

        public InvalidArmorException(string message) : base(String.Format(message))
        {

        }
    }
}
