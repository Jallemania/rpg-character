﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPG_character.Const
{
    public class Enums
    {
        public enum HeroClass
        {
            Mage,
            Ranger,
            Rouge,
            Warrior
        }

        public enum WeaponType
        {
            Axe,
            Bow,
            Dagger,
            Hammer,
            Staff,
            Sword,
            Wand
        }

        public enum ArmorType
        {
            Cloth,
            Leather,
            Mail,
            Plate
        }

        public enum Slot
        {
            Head,
            Body,
            Legs,
            Weapon
        }
    }
}
