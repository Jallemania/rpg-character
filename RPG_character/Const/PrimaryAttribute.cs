﻿using RPG_character.Const;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPG_character
{
    public class PrimaryAttribute
    {
        public double Strength { get; set; }
        public double Dexterity { get; set; }
        public double Intelligence { get; set; }

        public PrimaryAttribute(double strength, double dexterity, double intelligence)
        {
            Strength = strength;
            Dexterity = dexterity;  
            Intelligence = intelligence;
        }

        public PrimaryAttribute()
        {

        }

        /// <summary>
        /// Takes in an enum that defines which hero class will be leveled up and then increases each stat based on class.
        /// </summary>
        /// <param name="heroClass"></param>
        /// <returns>An updated instance of the primary attributes with properties increased based on character class</returns>
        public PrimaryAttribute LevelUp(Enums.HeroClass heroClass)
        {
            switch (heroClass)
            {
                case Enums.HeroClass.Mage:
                    Strength += 1d;
                    Dexterity += 1d;
                    Intelligence += 5d;
                    return this;
                case Enums.HeroClass.Ranger:
                    Strength += 1d;
                    Dexterity += 5d;
                    Intelligence += 1d;
                    return this;
                case Enums.HeroClass.Rouge:
                    Strength += 1d;
                    Dexterity += 4d;
                    Intelligence += 1d;
                    return this;
                case Enums.HeroClass.Warrior:
                    Strength += 3d;
                    Dexterity += 2d;
                    Intelligence += 1d;
                    return this;
                default:
                    break;
            }

            return this;
        }

        /// <summary>
        /// Takes in the hero class and returns the attribute tied with that class.
        /// </summary>
        /// <param name="heroClass"></param>
        /// <returns>A double value representing the characters class attribute.</returns>
        public double GetClassPrimary(Enums.HeroClass heroClass)
        {
            switch (heroClass)
            {
                case Enums.HeroClass.Mage:
                    return Intelligence;
                case Enums.HeroClass.Ranger:
                    return Dexterity;
                case Enums.HeroClass.Rouge:
                    return Dexterity;
                case Enums.HeroClass.Warrior:
                    return Strength;
                default:
                    return 0;
            }
        }

        /// <summary>
        /// Takes in an instance of this class and adds the content to the current instance.
        /// </summary>
        /// <returns>A new instance of PrimaryAttributes with updates values.</returns>
        public PrimaryAttribute Add(PrimaryAttribute attributes)
        {
            Strength += attributes.Strength;
            Dexterity += attributes.Dexterity;
            Intelligence += attributes.Intelligence;

            return this;
        }

        /// <summary>
        /// Compares the properties of two PrimaryAttribute objects too see if they have the same values.
        /// Used to test equality in unit tests.
        /// </summary>
        /// <param name="attributes"></param>
        /// <returns>A bool based on if values are equal or not.</returns>
        public bool IsEqual(PrimaryAttribute attributes)
        {
            if(this.Strength == attributes.Strength && this.Dexterity == attributes.Dexterity && this.Intelligence == attributes.Intelligence)
            {
                return true;
            }

            return false;
        } 
    }
}
