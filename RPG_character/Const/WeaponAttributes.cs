﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPG_character.Const
{
    public class WeaponAttributes
    {
        public double BaseDamage { get; set; }

        public double AttacksPerSecond { get; set; }

        public WeaponAttributes()
        {

        }
    }
}
