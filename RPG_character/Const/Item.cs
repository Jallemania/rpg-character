﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPG_character.Const
{
    public class Item
    {
        public string Name { get; set; }
        public int RequiredLevel { get; set; }

        public Item(string name, int reqLvl)
        {
            Name = name;
            RequiredLevel = reqLvl;

        }

        public Item()
        {
            Name = "Name not set";
        }
    }
}
