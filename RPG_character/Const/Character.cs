﻿using RPG_character.Equipment;
using RPG_character.Const;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPG_character.Const
{
    public abstract class Character
    {
        
        public int Level { get; set; }
        public string Name { get; set; }
        public Dictionary<Enums.Slot, Item> Equipment { get; set; }
        public PrimaryAttribute PrimaryAttributes { get; set; }
        public PrimaryAttribute AttributesTotal { get; set; }
        public double Damage { get; set; }

        public Character()
        {
            Name = "Name not set";
            Equipment = new Dictionary<Enums.Slot, Item>();
            PrimaryAttributes = new PrimaryAttribute();
            AttributesTotal = new PrimaryAttribute();
        }
        public Character(string name, Enums.HeroClass heroClass)
        {
            Level = 1;
            Name = name;
            Equipment = new Dictionary<Enums.Slot, Item>();
            AttributesTotal = new PrimaryAttribute();

            switch (heroClass)
            {
                case Enums.HeroClass.Mage:
                    PrimaryAttributes = new PrimaryAttribute(1, 1, 8);
                    break;
                case Enums.HeroClass.Ranger:
                    PrimaryAttributes = new PrimaryAttribute(1, 7, 1);
                    break;
                case Enums.HeroClass.Rouge:
                    PrimaryAttributes = new PrimaryAttribute(2, 6, 1);
                    break;
                case Enums.HeroClass.Warrior:
                    PrimaryAttributes = new PrimaryAttribute(5, 2, 1);
                    break;
                default:
                    PrimaryAttributes = new PrimaryAttribute(0, 0, 0);
                    break;
            }

            UpdateStats(heroClass);
        }

        /// <summary>
        /// Builds a StringBuilder object containing stats about the character build. 
        /// Then prints this information to the console.
        /// </summary>
        /// <param name="heroClass"></param>
        public void DisplayStats(Enums.HeroClass heroClass)
        {
            StringBuilder display = new StringBuilder();
            display.AppendLine($"------------------ {Name} ------------------");
            display.AppendLine($"Hero Class: {heroClass}");
            display.AppendLine($"Level: {Level}");
            display.AppendLine($"Strength: {AttributesTotal.Strength}");
            display.AppendLine($"Dexterity: {AttributesTotal.Dexterity}");
            display.AppendLine($"Intelligence: {AttributesTotal.Intelligence}");
            display.AppendLine($"Damage: {Damage}");

            Console.WriteLine(display);
        }

        /// <summary>
        /// Updates Character AttributesTotal, and Damage output when called.
        /// </summary>
        public void UpdateStats(Enums.HeroClass heroClass)
        {
            AttributesTotal = PrimaryAttributes;
            GetArmorAttributes();
            Damage = GetCharacterDamage(heroClass);
        }
        
        /// <summary>
        /// Checks if a weapon is equipped and updates character damage based on weapon stats.
        /// </summary>
        /// <returns>A double representing character damage.</returns>
        private double GetCharacterDamage(Enums.HeroClass heroClass)
        {
            if (Equipment.TryGetValue(Enums.Slot.Weapon, out Item? item))
            {
                switch (item)
                {
                    case Weapon weapon:
                        double damage = weapon.DamagePerSecond * (1d + (AttributesTotal.GetClassPrimary(heroClass) / 100d));
                        return damage;
                    default:
                        break;
                }
            }

            return 1d * (1d + (5d / 100d));
        }

        /// <summary>
        /// Iterates through the Equipment dictionary and checks if the entry is type Armor, 
        /// then adds its base attributes to the AttributesTotal.
        /// </summary>
        private void GetArmorAttributes()
        {

            foreach (KeyValuePair<Enums.Slot, Item> entry in Equipment)
            {
                switch (entry.Value)
                {
                    case Armor armor:
                        AttributesTotal.Add(armor.BaseAttributes);
                        break;
                    default:
                        break;
                }
            }
             
        }
        
        /// <summary>
        /// Updates character properties of 'Level' and 'PrimaryAttributes'. 
        /// Uses an input enum to determine which hero class will be leveled up.
        /// </summary>
        /// <param name="heroClass"></param>
        public void LevelUp(Enums.HeroClass heroClass)
        {
            Level++;
            PrimaryAttributes.LevelUp(heroClass);
            UpdateStats(heroClass);
        }

        /// <summary>
        /// Takes in a Weapon object and checks if it is compatible with the current hero class and required level is met. 
        /// Will equip in weapon slot if compatible, throws an InvalidArmorException if not. 
        /// </summary>
        /// <param name="weapon"></param>
        public abstract string EquipItem(Weapon weapon);

        /// <summary>
        /// Takes in an Armor object and equips it in the specified slot if its compatible with hero class and required level is met.
        /// Throws an InvalidArmorException otherwise.
        /// </summary>
        /// <param name="armor"></param>
        /// <param name="slot"></param>
        public abstract string EquipItem(Armor armor);


        /// <summary>
        /// Takes in a Weapon object and checks the type of said weapon to see 
        /// if it is compatible with the hero class.
        /// </summary>
        /// <param name="weapon"></param>
        /// <returns>True based on if the weapon was compatible. Throws an InvalidWeaponException otherwise.</returns>
        public abstract bool CheckWeaponCompatability(Weapon weapon);


        /// <summary>
        /// Takes in an Armor object and checks the type of said armor to see 
        /// if it is compatible with the hero class.
        /// </summary>
        /// <param name="armor"></param>
        /// <returns>True if compatible. Throws an InvalidArmorException otherwise.</returns>
        public abstract bool CheckArmorCompability(Armor armor);
        
    }
}
