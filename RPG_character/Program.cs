﻿using System;
using RPG_character.CharacterClasses;
using RPG_character.Const;
using RPG_character.Equipment;

namespace RPG_character
{
    internal class Program
    {
        static void Main(string[] args)
        {
            Armor testPlateBody = new Armor()
            {
                Name = "Common plate body armor",
                RequiredLevel = 1,
                Slot = Enums.Slot.Body,
                Type = Enums.ArmorType.Plate,
                BaseAttributes = new PrimaryAttribute(1, 0, 0)
            };

            Weapon testAxe = new Weapon()
            {
                Name = "Common Axe",
                RequiredLevel = 1,
                Slot = Enums.Slot.Weapon,
                Type = Enums.WeaponType.Axe,
                Attributes = new WeaponAttributes()
                {
                    BaseDamage = 7,
                    AttacksPerSecond = 1.1
                }
            };
            
            Weapon testAxe2 = new Weapon()
            {
                Name = "Common Axe again",
                RequiredLevel = 1,
                Slot = Enums.Slot.Weapon,
                Type = Enums.WeaponType.Axe,
                Attributes = new WeaponAttributes()
                {
                    BaseDamage = 7,
                    AttacksPerSecond = 1.1
                }
            };


            Warrior coolMan = new Warrior("Peter");

            coolMan.EquipItem(testAxe);
            coolMan.EquipItem(testAxe2);
            coolMan.EquipItem(testPlateBody);

            coolMan.DisplayStats();
        }
    }
}
