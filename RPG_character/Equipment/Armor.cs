﻿using RPG_character.Const;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPG_character.Equipment
{
    public class Armor : Item
    {

        public PrimaryAttribute BaseAttributes { get; set; }

        public Enums.ArmorType Type { get; set; }

        public Enums.Slot Slot { get; set; }

        /// <summary>
        /// Uses the reqLvl param to give random base attributes.
        /// </summary>
        /// <param name="name"></param>
        /// <param name="reqLvl"></param>
        /// <param name="type"></param>
        /// <param name="slot"></param>
        public Armor(string name, int reqLvl, Enums.ArmorType type, Enums.Slot slot) : base(name, reqLvl)
        {
            Random rn = new Random();  
            Type = type;
            Slot = slot;

            if (reqLvl > 0 && reqLvl <= 5)
            {
                BaseAttributes = new PrimaryAttribute(rn.Next(1, 6), rn.Next(1, 6), rn.Next(1, 6));
            }
            else if (reqLvl > 5 && reqLvl <= 10)
            {
                BaseAttributes = new PrimaryAttribute(rn.Next(1, 6), rn.Next(5, 11), rn.Next(5, 11));
            }
            else if (reqLvl > 10 && reqLvl <= 15)
            {
                BaseAttributes = new PrimaryAttribute(rn.Next(10, 16), rn.Next(10, 16), rn.Next(10, 16));
            }
            else
            {
                BaseAttributes = new PrimaryAttribute(0, 0, 0);
            }
        }

        public Armor()
        {
            BaseAttributes = new PrimaryAttribute(0, 0, 0);
        }
    }
}
