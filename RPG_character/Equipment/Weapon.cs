﻿using RPG_character.Const;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPG_character.Equipment
{
    public class Weapon : Item
    {
        public Enums.WeaponType Type { get; set; }

        public Enums.Slot Slot { get; set; }

        public WeaponAttributes Attributes { get; set; }

        public double DamagePerSecond { get => Attributes.BaseDamage * Attributes.AttacksPerSecond;  }

        /// <summary>
        /// Uses the reqLvl param to give random base attributes.
        /// </summary>
        /// <param name="name"></param>
        /// <param name="reqLvl"></param>
        /// <param name="weaponType"></param>
        public Weapon(string name, int reqLvl, Enums.WeaponType weaponType) : base(name, reqLvl)
        {
            Random rn = new Random();
            Type = weaponType;
            Slot = Enums.Slot.Weapon;

            if (reqLvl > 0 && reqLvl <= 5)
            {
                Attributes = new WeaponAttributes()
                {
                    BaseDamage = rn.Next(1, 6),
                    AttacksPerSecond = rn.Next(1, 6),
                };
            }
            else if (reqLvl > 5 && reqLvl <= 10)
            {
                Attributes = new WeaponAttributes()
                {
                    BaseDamage = rn.Next(5, 11),
                    AttacksPerSecond = rn.Next(5, 11)
                };
            }
            else if (reqLvl > 10 && reqLvl <= 15)
            {
                Attributes = new WeaponAttributes()
                {
                    BaseDamage = rn.Next(10, 16),
                    AttacksPerSecond = rn.Next(10, 16)
                };
            }
            else
            {
                Attributes = new WeaponAttributes();
            }
            
        }

        public Weapon()
        {
            Attributes = new WeaponAttributes();
        }
    }
}
