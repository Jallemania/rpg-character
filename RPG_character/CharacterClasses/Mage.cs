﻿using RPG_character.Const;
using RPG_character.Equipment;
using RPG_character.Exceptions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPG_character.CharacterClasses
{
    public class Mage : Character
    {
        public Enums.HeroClass HeroClass { get; set; }

        public Mage()
        {

        }

        public Mage(string name) : base(name, Enums.HeroClass.Mage)
        {
            HeroClass = Enums.HeroClass.Mage;
        }

        public void DisplayStats() => base.DisplayStats(HeroClass);

        public void LevelUp() => base.LevelUp(HeroClass);

        public override string EquipItem(Weapon weapon)
        {
            if (CheckWeaponCompatability(weapon))
            {
                base.Equipment[weapon.Slot] = weapon;
                base.UpdateStats(HeroClass);

                return "New weapon equipped!";
            }

            return "Could not equip weapon.";
        }

        public override string EquipItem(Armor armor)
        {
            if (armor.Slot == Enums.Slot.Weapon) throw new InvalidArmorException("Type of Armor can not be equipped in Weapons slot");

            if (CheckArmorCompability(armor))
            {
                base.Equipment[armor.Slot] = armor;
                base.UpdateStats(HeroClass);

                return "New armor equipped!";
            }

            return "Could not equip armor.";
        }

        public override bool CheckWeaponCompatability(Weapon weapon)
        {

            InvalidWeaponException exception;

            if (weapon.Type == Enums.WeaponType.Staff || weapon.Type == Enums.WeaponType.Wand)
            {
                if (weapon.RequiredLevel <= base.Level)
                {
                    return true;
                }

                throw exception = new InvalidWeaponException("Hero class with level: " + base.Level + " is not able to equip this item. Required Level: " + weapon.RequiredLevel);
            }

            throw exception = new InvalidWeaponException("Hero class of type: " + HeroClass + " can't use weapon of type: " + weapon.Type);
        }

        public override bool CheckArmorCompability(Armor armor)
        {
            InvalidArmorException exception;

            if (armor.Type == Enums.ArmorType.Cloth)
            {
                if (armor.RequiredLevel <= base.Level)
                {
                    return true;
                }

                throw exception = new InvalidArmorException("Hero class with level: " + base.Level + " is not able to equip this item. Required Level: " + armor.RequiredLevel);
            }

            throw exception = new InvalidArmorException("Hero class: " + HeroClass + " can't equip armor type of: " + armor.Type);
        }
    }
}
