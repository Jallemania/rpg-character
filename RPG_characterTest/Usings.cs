global using Xunit;
global using RPG_character;
global using RPG_character.Const;
global using RPG_character.Equipment;
global using RPG_character.Exceptions;
global using RPG_character.CharacterClasses;