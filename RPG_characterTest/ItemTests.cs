﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPG_characterTest
{
    public class ItemTests
    {
        Weapon testAxe = new Weapon()
        {
            Name = "Common Axe",
            RequiredLevel = 1,
            Slot = Enums.Slot.Weapon,
            Type = Enums.WeaponType.Axe,
            Attributes = new WeaponAttributes()
            {
                BaseDamage = 7,
                AttacksPerSecond = 1.1
            }
        };
        
        Weapon testBow = new Weapon()
        {
            Name = "Common bow",
            RequiredLevel = 1,
            Slot = Enums.Slot.Weapon,
            Type = Enums.WeaponType.Bow,
            Attributes = new WeaponAttributes()
            {
                BaseDamage = 12,  
                AttacksPerSecond = 0.8
            }
        };

        Armor testPlateBody = new Armor()
        {
            Name = "Common plate body armor",
            RequiredLevel = 1,
            Slot = Enums.Slot.Body,
            Type = Enums.ArmorType.Plate,
            BaseAttributes = new PrimaryAttribute(1, 0, 0)
        };
        
        Armor testClothHead = new Armor()
        {
            Name = "Magic cloth head armor",
            RequiredLevel = 1,
            Slot = Enums.Slot.Head,
            Type = Enums.ArmorType.Cloth,
            BaseAttributes = new PrimaryAttribute(0, 0, 5) 
        };


        [Fact]
        public void Invalid_Weapon_Level()
        {
            testAxe.RequiredLevel = 2;
            Warrior warrior = new Warrior("Johnny");
            InvalidWeaponException expected = new InvalidWeaponException("Hero class with level: " + warrior.Level + " is not able to equip this item. Required Level: " + testAxe.RequiredLevel);

            InvalidWeaponException result = Assert.Throws<InvalidWeaponException>(() => warrior.EquipItem(testAxe));

            Assert.Equal(expected.Message, result.Message);
            Assert.Equal(expected.GetType(), result.GetType()); 
        }
        
        [Fact]
        public void Invalid_Weapon_Type()
        {
            Warrior warrior = new Warrior("Bethel");
            InvalidWeaponException expected = new InvalidWeaponException("Hero class of type: " + warrior.HeroClass + " can't use weapon of type: " + testBow.Type);

            InvalidWeaponException result = Assert.Throws<InvalidWeaponException>(() => warrior.EquipItem(testBow));

            Assert.Equal(expected.Message, result.Message);
            Assert.Equal(expected.GetType(), result.GetType());
        } 
        
        [Fact]
        public void Invalid_Armor_Level()
        {
            testPlateBody.RequiredLevel = 2;
            Warrior warrior = new Warrior("Johnny");
            InvalidArmorException expected = new InvalidArmorException("Hero class with level: " + warrior.Level + " is not able to equip this item. Required Level: " + testPlateBody.RequiredLevel);

            InvalidArmorException result = Assert.Throws<InvalidArmorException>(() => warrior.EquipItem(testPlateBody));

            Assert.Equal(expected.Message, result.Message);
            Assert.Equal(expected.GetType(), result.GetType());
        }
        
        [Fact]
        public void Invalid_Armor_Type()
        {
            Warrior warrior = new Warrior("Bethel");
            InvalidArmorException expected = new InvalidArmorException("Hero class: " + warrior.HeroClass + " can't equip armor type of: " + testClothHead.Type);

            InvalidArmorException result = Assert.Throws<InvalidArmorException>(() => warrior.EquipItem(testClothHead));

            Assert.Equal(expected.Message, result.Message);
            Assert.Equal(expected.GetType(), result.GetType());
        }

        [Fact]
        public void Equip_Weapon_Success()
        {
            Warrior warrior = new Warrior("Steve");
            string expected = "New weapon equipped!";

            string actual = warrior.EquipItem(testAxe);

            Assert.Equal(expected, actual);
        }

        [Fact]
        public void Equip_Armor_Success()
        {
            Warrior warrior = new Warrior("Steve");
            string expected = "New armor equipped!";

            string actual = warrior.EquipItem(testPlateBody);

            Assert.Equal(expected, actual);
        }

        [Fact]
        public void Total_Attributes_With_Armor()
        {
            Warrior warrior = new Warrior("Steve");
            warrior.EquipItem(testPlateBody);
            PrimaryAttribute exp = new PrimaryAttribute(6, 2, 1);

            PrimaryAttribute act = warrior.AttributesTotal;

            Assert.True(exp.IsEqual(act));
        }

        [Fact]
        public void Damage_No_Weapon()
        {
            Warrior warrior = new Warrior("Steve");
            double expected = 1d * (1d + (5d / 100d));

            double actual = warrior.Damage;

            Assert.True(actual == expected);
        }

        [Fact]
        public void Damage_With_Weapon()
        {
            Warrior warrior = new Warrior("Steve");
            warrior.EquipItem(testAxe);
            double expected = (7d * 1.1d) * (1d + (5d / 100d));

            double actual = warrior.Damage;

            Assert.Equal(expected, actual);
        }

        [Fact]
        public void Damage_With_Weapon_And_Armor()
        {
            Warrior warrior = new Warrior("Steve");
            warrior.EquipItem(testAxe);
            warrior.EquipItem(testPlateBody);
            double expected = (7d * 1.1d) * (1d + ((5d + 1d) / 100d));

            double actual = warrior.Damage;

            Assert.Equal(actual, expected);

        }

    }
}
