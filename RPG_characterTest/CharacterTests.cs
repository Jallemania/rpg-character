namespace RPG_characterTest
{
    public class CharacterTests
    {

        //methodName_conditions_expectedResult()
        [Fact]
        public void Level_Initial()
        {
            //ARRANGE
            Warrior warrior = new Warrior("Evan the Almighty");
            double expectedLevel = 1;

            //ACT
            double actualLevel = warrior.Level;

            //ASSERT
            Assert.True(expectedLevel == actualLevel);
        }

        [Fact]
        public void Level_Increase()
        {
            //ARRANGE
            Warrior warrior = new Warrior("Evan the Almighty");
            warrior.LevelUp();
            double expectedLevel = 2;

            //ACT
            double actualLevel = warrior.Level;

            //ASSERT
            Assert.True(expectedLevel == actualLevel);
        }

        [Fact]
        public void Warrior_Initial_Stats()
        {
            //ARRANGE
            Warrior warrior = new Warrior("Evan the Almighty");
            double expectedStrength = 5;
            double expectedDexterity = 2;
            double expectedIntelligence = 1;

            //ACT
            double actualStrength = warrior.PrimaryAttributes.Strength;
            double actualDexterity = warrior.PrimaryAttributes.Dexterity;
            double actualIntelligence = warrior.PrimaryAttributes.Intelligence;

            //ASSERT
            Assert.True(expectedStrength == actualStrength);
            Assert.True(expectedDexterity == actualDexterity);
            Assert.True(expectedIntelligence == actualIntelligence);
        }

        [Fact]
        public void Rouge_Initial_Stats()
        {
            //ARRANGE
            Rouge rouge = new Rouge("Evan the Almighty");
            double expectedStrength = 2;
            double expectedDexterity = 6;
            double expectedIntelligence = 1;

            //ACT
            double actualStrength = rouge.PrimaryAttributes.Strength;
            double actualDexterity = rouge.PrimaryAttributes.Dexterity;
            double actualIntelligence = rouge.PrimaryAttributes.Intelligence;

            //ASSERT
            Assert.True(expectedStrength == actualStrength);
            Assert.True(expectedDexterity == actualDexterity);
            Assert.True(expectedIntelligence == actualIntelligence);
        }

        [Fact]
        public void Ranger_Initial_Stats()
        {
            //ARRANGE
            Ranger ranger = new Ranger("Evan the Almighty");
            double expectedStrength = 1;
            double expectedDexterity = 7;
            double expectedIntelligence = 1;

            //ACT
            double actualStrength = ranger.PrimaryAttributes.Strength;
            double actualDexterity = ranger.PrimaryAttributes.Dexterity;
            double actualIntelligence = ranger.PrimaryAttributes.Intelligence;

            //ASSERT
            Assert.True(expectedStrength == actualStrength);
            Assert.True(expectedDexterity == actualDexterity);
            Assert.True(expectedIntelligence == actualIntelligence);
        }

        [Fact]
        public void Mage_Initial_Stats()
        {
            //ARRANGE
            Mage mage = new Mage("Evan the Almighty");
            double expectedStrength = 1;
            double expectedDexterity = 1;
            double expectedIntelligence = 8;

            //ACT
            double actualStrength = mage.PrimaryAttributes.Strength;
            double actualDexterity = mage.PrimaryAttributes.Dexterity;
            double actualIntelligence = mage.PrimaryAttributes.Intelligence;

            //ASSERT
            Assert.True(expectedStrength == actualStrength);
            Assert.True(expectedDexterity == actualDexterity);
            Assert.True(expectedIntelligence == actualIntelligence);
        }

        [Fact]
        public void Warrior_Level_Up_Stats()
        {
            //ARRANGE
            Warrior warrior = new Warrior("Evan the Almighty");
            warrior.LevelUp();
            double expectedStrength = 5 + 3;
            double expectedDexterity = 2 + 2;
            double expectedIntelligence = 1 + 1;

            //ACT
            double actualStrength = warrior.PrimaryAttributes.Strength;
            double actualDexterity = warrior.PrimaryAttributes.Dexterity;
            double actualIntelligence = warrior.PrimaryAttributes.Intelligence;

            //ASSERT
            Assert.True(expectedStrength == actualStrength);
            Assert.True(expectedDexterity == actualDexterity);
            Assert.True(expectedIntelligence == actualIntelligence);
        }

        [Fact]
        public void Rouge_Level_Up_Stats()
        {
            //ARRANGE
            Rouge rouge = new Rouge("Evan the Almighty");
            rouge.LevelUp();
            double expectedStrength = 2 + 1;
            double expectedDexterity = 6 + 4;
            double expectedIntelligence = 1 + 1;

            //ACT
            double actualStrength = rouge.PrimaryAttributes.Strength;
            double actualDexterity = rouge.PrimaryAttributes.Dexterity;
            double actualIntelligence = rouge.PrimaryAttributes.Intelligence;

            //ASSERT
            Assert.True(expectedStrength == actualStrength);
            Assert.True(expectedDexterity == actualDexterity);
            Assert.True(expectedIntelligence == actualIntelligence);
        }

        [Fact]
        public void Ranger_Level_Up_Stats()
        {
            //ARRANGE
            Ranger ranger = new Ranger("Evan the Almighty");
            ranger.LevelUp();
            double expectedStrength = 1 + 1;
            double expectedDexterity = 7 + 5;
            double expectedIntelligence = 1 + 1;

            //ACT
            double actualStrength = ranger.PrimaryAttributes.Strength;
            double actualDexterity = ranger.PrimaryAttributes.Dexterity;
            double actualIntelligence = ranger.PrimaryAttributes.Intelligence;

            //ASSERT
            Assert.True(expectedStrength == actualStrength);
            Assert.True(expectedDexterity == actualDexterity);
            Assert.True(expectedIntelligence == actualIntelligence);
        }

        [Fact]
        public void Mage_Level_Up_Stats()
        {
            //ARRANGE
            Mage mage = new Mage("Evan the Almighty");
            mage.LevelUp();
            double expectedStrength = 1 + 1;
            double expectedDexterity = 1 + 1;
            double expectedIntelligence = 8 + 5;

            //ACT
            double actualStrength = mage.PrimaryAttributes.Strength;
            double actualDexterity = mage.PrimaryAttributes.Dexterity;
            double actualIntelligence = mage.PrimaryAttributes.Intelligence;

            //ASSERT
            Assert.True(expectedStrength == actualStrength);
            Assert.True(expectedDexterity == actualDexterity);
            Assert.True(expectedIntelligence == actualIntelligence);
        }
    }
}